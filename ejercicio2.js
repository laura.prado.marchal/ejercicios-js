// - 1) Si el diámetro es **inferior o igual** a 10, la función deberá imprimir por consola "es una rueda para un juguete pequeño"
// - 2) Si el diámetro es **superior** a 10 y **menor** de 20, la función deberá imprimir por consola "es una rueda para un juguete mediano"
// - 3) Si el diámetro es **superior o igual** a 20, la función deberá imprimir por consola "es una rueda para un juguete grande"

function TipoRueda(tamaño) {
    if (tamaño <= 10) {
        console.log('Es una rueda para un juguete pequeño');
    }
    if (tamaño > 10 && tamaño < 20) {
        console.log('Es una rueda para un juguete mediano');
    }
    if (tamaño >= 20) {
        console.log('Es una rueda para un juguete grande')
    }
}

TipoRueda(17);
TipoRueda(5);
TipoRueda(50);