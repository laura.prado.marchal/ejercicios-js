// 1. Solicite al usuario su número de DNI y su letra. Debes realizar las preguntas por separado, es decir que tendrá que introducir dos datos, no uno. 
// 2. En primer lugar (y en una sola instrucción) se debe comprobar si el número es menor que `0` o mayor que `99999999`. Si ese es el caso, se muestra un mensaje al usuario indicando que el número proporcionado no es válido y el programa no muestra más mensajes.
// 3. Si el número es válido (paso), debes comprobar si la letra indicada por el usuario es correcta, para ello sigue el método explicado anteriormente. Deberás indicar al usuario el resultado de la validación, imprimiendo por pantalla "DNI Válido" o "DNI Incorrecto".

var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var NumDNI = prompt('Introduce el número de tu DNI');
var LetraDNI = prompt('Introduce la letra de tu DNI');


function VerLetra() {
    if (NumDNI < 0 || NumDNI > 99999999) {
        console.log('el número proporcionado no es válido')
    }

        else {
            var resto = NumDNI%23

            if (LetraDNI === letras[resto]) {
                console.log('El DNI es correcto');
            }
                else{
                    console.log('El DNI es incorrecto');
                }

        }

}

VerLetra();
